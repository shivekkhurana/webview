import React, {useState, useEffect, createRef} from 'react';
import {WebView} from 'react-native-webview';
import {TextInput, View, Button, BackHandler} from 'react-native';

export default function App() {
  const [uri, setUri] = useState("https://google.com");
  const [showWebView, setShowWebView] = useState(false);
  const webViewRef = createRef();

  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (showWebView) webViewRef.current.goBack();
      return showWebView;
    });

    return () => {
      backHandler.remove();
    }
  }, [webViewRef]);

  if(showWebView)
    return (<WebView source={{uri}} ref={webViewRef} />);
  else
    return (<View style={{marginTop: 24, padding: 8}}>
      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 4, padding: 8, marginBottom: 16}}
        onChangeText={(text) => setUri(text)}
        value={uri}
      />
      <Button
        onPress={() => setShowWebView(true)}
        title="Go"
        accessibilityLabel="Enter a url to open in webview"
      />

    </View>);
} 